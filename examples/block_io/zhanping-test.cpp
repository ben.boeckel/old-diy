//---------------------------------------------------------------------------
//
// example of using DIY to perform blocking, assignment of blocks to
//  processes, reading blocks from storage, creating a custom datatype
//  for the block data, and using it to write the data out to storage
//  as a vector of the original data points
//
// Tom Peterka
// Argonne National Laboratory
// 9700 S. Cass Ave.
// Argonne, IL 60439
// tpeterka@mcs.anl.gov
//
// (C) 2011 by Argonne National Laboratory.
// See COPYRIGHT in top-level directory.
//
//--------------------------------------------------------------------------
#include <string.h>
#include "mpi.h"
#include "diy.h"


// ZPL begin
//
// to vertify the effectiveness of the BIL-based parallel data loader, two
// runs are needed:               mpiexec -n 4 ./example
//
// (1) first  run: turn on  _EXPORT_BLOCKS_ and turn off _BLOCKS2VOLUME_
//
// (2) second run: turn off _EXPORT_BLOCKS_ and turn on  _BLOCKS2VOLUME_
//
// then compare the two files:    diff test.dat test_recon.dat
//

//#define  _EXPORT_BLOCKS_	// mutually EXCLUSIVE with _BLOCKS2VOLUME_
//#define  _BLOCKS2VOLUME_	// mutially EXCLUSIVE with _EXPORT_BLOCKS_


// -----------------------------------------------------------------------
//
// given a block data file, reconstruct the corresponding part of the volume
//
// invoked by process #0 only
//
// vol_data (for the whole volume) has been allocated with the exact size
// (which is volSizes[0] * volSizes[1] * volSizes[2]) by the caller
//
// added by Zhanping Liu on 07/10/2013
//
void	BlockBack2Volume( char * blckFile, int * volSizes, int * vol_data )
{
	// block info
	int	shownSiz = 0;	  // directly from the block-file header
	int	reconSiz = 0;	  // derived  from the min and max of the block
	int	blckSizs[3];	  // derived  from the min and max of the block
	int     blckMins[3];      // directly from the block-file header
	int	blckMaxs[3];      // directly from the block-file header
	int   * blckData = NULL;  // array of values for THIS block only
	

	// read the block header and data
	FILE  *	in_block = fopen( blckFile, "rb" );
	fread(  blckMins,  sizeof( int ),  3,  in_block  );
	fread(  blckMaxs,  sizeof( int ),  3,  in_block  );
	fread( &shownSiz,  sizeof( int ),  1,  in_block  );

	blckSizs[0] = blckMaxs[0] - blckMins[0] + 1;
	blckSizs[1] = blckMaxs[1] - blckMins[1] + 1;
	blckSizs[2] = blckMaxs[2] - blckMins[2] + 1;
	reconSiz    = blckSizs[0] * blckSizs[1] * blckSizs[2];
	assert( shownSiz == reconSiz );

	blckData    = new int [ shownSiz ];
	fread(  blckData,  sizeof( int ),  shownSiz,  in_block  );
	fclose( in_block );         in_block = NULL;


	// use the block to fill in the corresponding part of the volume
        //
        // every value (point) of the block is placed back to the  EXACT 
        // location of the whole volume 
	//
	int	sliceSiz = volSizes[0] * volSizes[1];	
	int *	src_data = blckData;	  // use the block as the source
	int *	des_data = NULL;

	for ( int k = 0; k < blckSizs[2]; k ++ )
	{	
		int	k_offset = sliceSiz * ( blckMins[2] + k );
		for ( int j = 0; j < blckSizs[1]; j ++ )
		{
			int	j_offset = volSizes[0] * ( blckMins[1] + j );
			for ( int i = 0; i < blckSizs[0]; i ++, src_data ++ )
			{
				// source:      the block
				// destination: the volume
				des_data =  vol_data + k_offset + j_offset + ( blckMins[0] + i );
			       *des_data = *src_data;
			}
		}
	}

	src_data = NULL;	des_data = NULL;


	// release memory
	delete [] blckData;	blckData = NULL;
}


// put a set of blocks (resulting from DIY decomposition of a volume) back  into
// the whole volume for reconstruction and write the reconstructed volume out to
// a disk BINARY file,  which will be compared against  the original volume file
//
// invoked by process #0 only
//
// added by Zhanping Liu on 07/10/2013
//
void	ReconstructVolume( int * volSizes )
{
	// block file names
	char	blkFiles[8][200] = {
					"block_0_0.dat",	"block_0_1.dat",
					"block_1_0.dat",	"block_1_1.dat",
					"block_2_0.dat",	"block_2_1.dat",
					"block_3_0.dat",	"block_3_1.dat"
				   };

	// volume size and its array
	int     vol_size = volSizes[0] * volSizes[1] * volSizes[2];
	int *	recnData = new int [ vol_size ];

   	// reconstruct the volume
  	for ( int i = 0; i < 8; i ++ )
  	BlockBack2Volume( blkFiles[i], volSizes, recnData );

	// write the volume to a disk file
	FILE  *	out_file = fopen( "test_recon.dat", "wb" );
	fwrite( recnData, sizeof( int ), vol_size, out_file);
	fclose( out_file );

	delete [] recnData;	         recnData = NULL;
}

//
// ZPL end


//
// user-defined callback function for creating a DIY datatype for
//   writing a block
//
// item: pointer to the item
// did: domain id
// lid: local block number
// dtype: pointer to the datatype
//
// side effects: commits the DIY datatype but DIY will cleanup datatype for you
//
void CreateWriteType(void *item, int did, int lid, DIY_Datatype *dtype) {

  // in this example we're just writing a vector af all the data points
  // in the original data block
  int min[3], size[3]; // block extents
  DIY_Block_starts_sizes(did, lid, min, size);
  int block_size = size[0] * size[1] * size[2];
  DIY_Create_vector_datatype(block_size, 1, DIY_INT, dtype);

}

//
// user-defined callback function for allocaing a block and 
//   creating a DIY datatype for reading the block
//
// did: domain id
// lid: local block number
// hdr: block header (not used in this example)
// base_addr: base address associated with the datatype
// dtype: pointer to the datatype
//
// side effects: allocates space for block
//  commits the DIY datatype but DIY will cleanup datatype for you
//
// returns: address of allocated block
//
void *CreateReadType(int did, int lid, int *hdr, DIY_Datatype *dtype) {

  // in this example we're just reading the vector af data points stored earlier
  // we'll need to allocate space for that vector and return that address

  // in a real problem, quantity information would be written in the header
  // in this example, we know the sizes from the original data block size
  int min[3], size[3]; // block extents
  DIY_Block_starts_sizes(did, lid, min, size);
  int block_size = size[0] * size[1] * size[2];

  // create datatype
  DIY_Create_vector_datatype(block_size, 1, DIY_INT, dtype);

  // allocate space and return its address
  int *read_buf = new int[block_size];
  return read_buf;

}

int main(int argc, char **argv) {

  int dim = 3;
  int tot_blocks = 8;
  int data_size[3] = {10, 10, 10}; // {x_size, y_size, z_size}
  int given[3] = {0, 0, 0}; // no constraints on decomposition in {x, y, z}
  int ghost[6] = {0, 0, 0, 0, 0, 0}; // -x, +x, -y, +y, -z, +z ghost
  int min[3], max[3], size[3]; // block extents
  char *infiles[] = { (char *)"test.dat" }; // pre-made 10x10x10 ints
  char outfile[] = "test.out"; // analysis output file
  int num_threads = 4; // number of threads DIY can use
  int nblocks; // my local number of blocks
  int rank; // MPI process
  int did; // domain id

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);


  // reconstruct the volume by placing a set of blocks back to ZPL begin
  // the exact part of the volume
  //
  #ifdef _BLOCKS2VOLUME_
  if ( rank == 0 ) ReconstructVolume( data_size );
  #endif
  // ZPL end


  // examples don't do any error checking, but real apps should

  // initialize DIY after initializing MPI
  DIY_Init(dim, num_threads, MPI_COMM_WORLD);

  // decompose domain
  did = DIY_Decompose(ROUND_ROBIN_ORDER, data_size, tot_blocks, &nblocks, 
		      1, ghost, given, 0);

  // allocate pointers to data, in this example, the data type is int
  // the memset to 0 is needed to tell DIY to allocate the memory for us
  int *data[nblocks];
  memset(data, 0, sizeof(int*) * nblocks);

  // read all data
  DIY_Read_data_all(did, infiles, DIY_INT, (void**)data, 0);

  // print the data values in the blocks
  for (int i = 0; i < nblocks; i++) {
    fprintf(stderr, "\nData values for local block %d global block %d:\n", 
	    i, DIY_Gid(did, i));
    DIY_Block_starts_sizes(did, i, min, size);
    for (int j = 0; j < size[0] * size[1] * size[2]; j++)
      fprintf(stderr, "%d ", data[i][j]);
    fprintf(stderr, "\n\n");
  }


  // write each block (coupled with a brief header as the meta info) ZPL begin
  // to a separate disk (BINARY) file
  //
  // the brief header is used to place this block (sub-volume) back
  // into the whole volume for reconstructing the exact part
  //
  // once ALL blocks (from multiple processes) are properly placed back,
  // possibly with minor overlap (due to ghost cells), the original volume
  // SHOULD be correctly reconstructed    IF    the blocks are loaded with
  // correct bytes (e.g., through the use of a BIL-based parallel reader)
  //
  #ifdef _EXPORT_BLOCKS_
  for ( int j = 0; j < nblocks; j ++ ) // j: local block ID
  {	
	// get the min and max of this block
	DIY_Block_starts_sizes( did, j, min, size );
	for ( int i = 0; i < 3; i ++ ) max[i] = min[i] + size[i] - 1;

	// number of values in this block and the block-file name
	int	  arraySize = size[0] * size[1] * size[2];  // number of values in this block
	char	  blockName[200];
	sprintf(  blockName, "block_%d_%d.dat", rank, j );

        // write the block (with a brief header) to a separate disk file
	FILE   *  theOutput = fopen( blockName, "wb" );
	fwrite(   min, 	      sizeof( int ),  3,  	  theOutput  );  // min of the block
	fwrite(   max,        sizeof( int ),  3,  	  theOutput  );  // max of the block
	fwrite(  &arraySize,  sizeof( int ),  1,          theOutput  );  // number of values in this block
	fwrite(   data[j],    sizeof( int ),  arraySize,  theOutput  );  // array of values
	fclose(   theOutput   );	
  }
  #endif 
  //*//// ZPL end


  // an example of using headers
  // write the size of each block in a header
  int **hdrs = new int*[nblocks];
  for (int i = 0; i < nblocks; i++) {
    hdrs[i] = new int[DIY_MAX_HDR_ELEMENTS];
    memset(hdrs[i], 0, DIY_MAX_HDR_ELEMENTS * sizeof(int)); // safety
    DIY_Block_starts_sizes(did, i, min, size);
    hdrs[i][0] = size[0] * size[1] * size[2];
  }

  // write the results
  // in this example, we are just writing the original data back out as
  // one vector per block
  if (rank == 0)
    fprintf(stderr, "\nWriting analysis results out to storage\n\n");
  DIY_Write_open_all(did, outfile, 0);
  DIY_Write_blocks_all(did, (void **)data, nblocks, hdrs, &CreateWriteType);
  DIY_Write_close_all(did);

  // read the results back in
  // demonstrates how some follow-on task would read analysis results
  // previously written in the DIY format
  void **analysis_blocks;
  int glo_analysis_blocks; // global number of blocks in the file
  int loc_analysis_blocks; // local number of blocks in the file
  DIY_Read_open_all(did, outfile, 0, 0, &glo_analysis_blocks, 
		    &loc_analysis_blocks);
  DIY_Read_blocks_all(did, &analysis_blocks, hdrs, &CreateReadType);
  DIY_Read_close_all(did);
  if (rank == 0)
    fprintf(stderr, "\nReading analysis results back from storage\n\n");
  for (int i = 0; i < loc_analysis_blocks; i++) {
    // Quantity information would normally be stored in a header
    // accompanying the block. Here we just recompute based on the
    // original data block size.
    DIY_Block_starts_sizes(did, i, min, size);
    int num_pts = size[0] * size[1] * size[2];
    fprintf(stderr, "\nAnalysis results read back into local block %d: ", i);
    for (int j = 0; j < num_pts; j++)
      fprintf(stderr, "%d ", ((int **)analysis_blocks)[i][j]);
    fprintf(stderr, "\n\n");
  }

  // cleanup
  for (int i = 0; i < nblocks; i++)
    delete[] hdrs[i];
  delete[] hdrs;

  // finalize DIY before finalizing MPI
  DIY_Finalize();

  MPI_Finalize();

  fflush(stderr);
  if (rank == 0)
    fprintf(stderr, "\n---Completed successfully---\n");
  return 0;

}
